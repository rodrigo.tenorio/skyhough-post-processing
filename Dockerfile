FROM docker.pkg.github.com/pyfstat/pyfstat/pyfstat:1.11.6

LABEL name="SkyHough O3 Analysis" \
      maintainer="Rodrigo Tenorio <rodrigo.tenorio@ligo.org"

COPY setup.py .
COPY local_utilities local_utilities
RUN pip install .
