import setuptools

setuptools.setup(
    name="local_utilities",
    packages=setuptools.find_packages(),
    install_requires=[
        "astropy",
        "bokeh",
        "htcondor",
        "jupyter",
        "matplotlib",
        "networkx",
        "numpy",
        "pandas",
        "pyfstat",
        "skyhoughtools @ git+https://rodrigo.tenorio:X6EXxpbxJBz6oEci8r5Z@git.ligo.org/rodrigo.tenorio/skyhough-tools",
    ],
)
