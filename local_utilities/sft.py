import os
import numpy as np

def get_sft_pattern(f0_Hz, base_folder, ifo, data_type, data_label,  t_coh):
    sft_pattern = os.path.join(
            *[base_folder, f"{ifo}_{data_type}_{data_label}_{t_coh}", f"{np.floor(f0_Hz)}Hz", "*.sft"]
            )
    return sft_pattern

def get_dirname_of_frequency_data(frequency_Hz, data_label, psd_bin_reduction=None):
    """
    Return the path to the proper frequency data.
    Prepend psd_data or sft_data with the propper root
    path to refer to one or the other one
    """
    f0 = np.floor(frequency_Hz, dtype=float)
    
    full_data_label = data_label + (f"_{psd_bin_reduction}" if psd_bin_reduction else '')
    dirname = f"{full_data_label}/{f0}Hz"
    return dirname

