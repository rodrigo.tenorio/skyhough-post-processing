import argparse
import logging


def condor_parse_python_args(argv=None):
    parser = argparse.ArgumentParser(description="Submit a python job using HTCondor")
    parser.add_argument("--python_script", help="Python script to be executed")
    parser.add_argument(
        "--python_args",
        help="Python script's arguments",
        nargs="*",
        default="",
    )

    return _parse(parser, argv)


def compute_psd_args(argv=None):
    parser = argparse.ArgumentParser(description="Arguments to run ComputePSD")
    parser.add_argument("--ifo", help="Detector to use. Comma-separated string")
    parser.add_argument(
        "--band_index", help="50Hz-frequency-band-index [0, ..., 9] to use", type=int
    )

    args = _parse(parser, argv)
    return args.ifo, args.band_index


def _parse(parser, argv):
    args = parser.parse_args(argv)
    _log_None_args(args)
    return args


def _log_None_args(args):
    for arg in vars(args):
        if getattr(args, arg) is None:
            logging.error("Argument {} was not set!".format(arg))
