import os
import numpy as np


def parse_cff(cff_file):

    translate = {
        "Freq": "F0",
        "refTime": "tref",
        "f1dot": "F1",
        "f2dot": "F2",
        "phi0": "phi",
    }

    signal_parameters = {}
    with open(cff_file, "r") as f:
        next(f)  # jump the first line
        for line in f:
            tokens = line.split(" = ")
            key, value = tokens[0], tokens[1]
            if key == "transientWindowType":
                continue
            key = translate.get(key, key)
            signal_parameters[key] = float(value)
    return signal_parameters


def get_skypatch_center(right_ascension, declination):
    return f'"({right_ascension}, {declination})"'


def check_if_patch_is_done(
    sky_patch, num_datasets, num_skypatches, fbasenameOut, dirnameOut
):

    # The output looks like this
    output_template = (
        "{}_Toplist_Dataset".format(fbasenameOut)
        + "_{}"
        + "_{}".format(num_datasets)
        + "_SkyPatch_{}_{}.dat".format(sky_patch, num_skypatches)
    )
    output_template = os.path.join(dirnameOut, output_template)

    return all(
        os.path.isfile(output_template.format(dataset))
        for dataset in range(num_datasets + 1)
    )


def compute_number_allsky_patches(d_skypatch_rad):
    """
    How many sky patches will be generated?
    Assume all sky isotropic grid and d_alpha = d_delta = d_skypatch_rad
    This has been soft-checked against O2 results.

    Taken from DopplerScan.c
    """

    # Start as DopplerScan.c. Points are going to be (RA, DEC)
    lower_left = np.array([1e-6, -1.570796])
    upper_right = np.array([6.283185, 1.570796])
    this_point = lower_left.copy()
    counter = 0

    while this_point[1] <= upper_right[1]:
        this_point[0] += d_skypatch_rad / np.cos(this_point[1])
        counter += 1
        if this_point[0] > upper_right[0]:
            this_point[0] = lower_left[0]
            this_point[1] += d_skypatch_rad

    return counter


def compute_sky_resolution(f0, t_coh, pixel_factor, v_over_c=1e-4):
    "This is dTheta in Krishnan et al."
    f0Bin = np.floor(f0 * t_coh + 0.5)
    sky_resolution = 1.0 / (v_over_c * pixel_factor * f0Bin)
    return sky_resolution


def compute_sky_patch_resolution(
    f0, t_coh, pixel_factor, sky_patch_side_bins, v_over_c=1e-4
):
    """
    Compute the length (rads) of a sky patch for a given number of
    sky bins. Length is conservatively cut off at 1 radian
    """
    sky_resolution = compute_sky_resolution(f0, t_coh, pixel_factor, v_over_c)

    # Reduce sky patch in steps of 0.5 until the number of bins
    # is satisfied. It allows for the imposition of a upper cutoff
    # (1. in this case).
    sky_patch_resolution = 1.0
    this_patch_size_rad = 4 * np.tan(0.25 * sky_patch_resolution)
    this_patch_side_bins = np.ceil(this_patch_size_rad / sky_resolution)
    while this_patch_side_bins > sky_patch_side_bins:
        sky_patch_resolution -= 0.05
        this_patch_size_rad = 4 * np.tan(0.25 * sky_patch_resolution)
        this_patch_side_bins = np.ceil(this_patch_size_rad / sky_resolution)

    return sky_patch_resolution
