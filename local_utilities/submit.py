import logging
import os
import subprocess
import sys

from local_utilities import parse


def craft_cmd(executable_path, args_dict):

    args = " ".join([f"--{key} {value}" for key, value in args_dict.items()])
    cmd = " ".join([executable_path, args])
    return cmd


def submit_subprocess(cmd, log_file):
    with open(log_file, "w") as f:
        f.write(cmd + "\n")
        f.flush()
        subprocess.run(cmd, stdout=f, stderr=f, shell=True)


def basic_submit_no_args(
    executable, request_memory=8000, accounting_group="ligo.dev.o3.cw.allskyisolated.skyhough"
):

    submit_dict = {
        "executable": executable,
        "request_memory": request_memory,
        "requirements": "",
        #"initialdir": sys.path[0],
        "on_exit_hold": "(ExitBySignal == True) || (ExitCode != 0)",
        "should_transfer_files": "IF_NEEDED",
        "accounting_group": accounting_group,
    }

    for other_keys in ["args", "output", "log", "error"]:
        submit_dict[other_keys] = "$({})".format(other_keys)

    return submit_dict
