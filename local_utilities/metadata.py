import json


def get_metadata(metadata_path=None):

    with open(metadata_path or "../metadata/metadata.json", "r") as f:
        metadata = json.load(f)

    return metadata
